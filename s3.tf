resource "aws_s3_bucket" "b" {
  bucket = "cicd-training-${data.aws_caller_identity.current.account_id}-${data.aws_region.current.name}"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}


data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

provider "aws" {
  region = "us-east-1"
}