# Use bucket from your own account
terraform {
  backend "s3" {
    bucket = "state-bucket-597429366363-us-east-1"
    key    = "policy/cicd-first-pipeline.tfstate"
    region = "us-east-1"
    # dynamodb_table = "terraform-lock"
  }
}
